# UDP Multiplexer in Rust

* Acts as a relay distributing incoming udp traffic to the specified destinations.
* Clients are remembered and outgoing traffic is relayed to all those clients

## Usage

```
udpmultiplexer 0.1.0
Philipp Richter <richterphilipp.pops@gmail.com>
Multiplexes UDP to different destinations

USAGE:
    udpmultiplexer [OPTIONS] <[ip:]port>...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -p, --port <port>        Port to listen on [default: 8000]
    -s, --source <source>    Source IP [default: 0.0.0.0]

ARGS:
    <[ip:]port>...    Default ip is 127.0.0.1

```

## License

MIT (c) 2018 Philipp Richter
