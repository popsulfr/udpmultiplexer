// MIT License

// Copyright (c) 2018 Philipp Richter

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#[macro_use]
extern crate clap;
extern crate num_cpus;

use std::error::Error;

const DEFAULT_SOURCE_IP: &'static str = "0.0.0.0";
const DEFAULT_SOURCE_PORT: &'static str = "8000";
const DEFAULT_DESTINATION_IP: &'static str = "127.0.0.1";

struct Args {
    source_addr: std::net::SocketAddr,
    dst_addrs: Vec<std::net::SocketAddr>,
}

struct Message {
    size: usize,
    data: [u8; std::u16::MAX as usize]
}

fn args_handler() -> Args {
    let matches = clap::App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .author(crate_authors!())
        .arg(clap::Arg::with_name("port")
             .help("Port to listen on")
             .short("p")
             .long("port")
             .takes_value(true)
             .default_value(DEFAULT_SOURCE_PORT)
             .validator(|s: String| -> Result<(),String> {
                   match s.parse::<u16>() {
                       Err(_) => return Err(String::from(
                             format!("Needs to be between {}-{}",
                                     std::u16::MIN,
                                     std::u16::MAX))),
                       _ => return Ok(()),
                   }
             }))
        .arg(clap::Arg::with_name("source")
             .help("Source IP")
             .short("s")
             .long("source")
             .takes_value(true)
             .default_value(DEFAULT_SOURCE_IP)
             .validator(|s: String| -> Result<(),String> {
                 match s.parse::<std::net::IpAddr>() {
                     Err(e) => return Err(String::from(e.description())),
                     _ => return Ok(()),
                 }
             }))
        .arg(clap::Arg::with_name("destinations")
             .help(&format!("Default ip is {}", DEFAULT_DESTINATION_IP))
             .min_values(1)
             .required(true)
             .value_name("[ip:]port")
             .validator(|s: String| -> Result<(),String> {
                 if s.contains(":") {
                     match s.parse::<std::net::SocketAddr>() {
                         Err(e) => return Err(String::from(e.description())),
                         _ => return Ok(()),
                     }
                 } else {
                     match s.parse::<u16>() {
                         Err(_) => return Err(String::from(
                                 format!("Needs to be between {}-{}",
                                         std::u16::MIN,
                                         std::u16::MAX))),
                         _ => return Ok(()),
                     }
                 }
             }))
        .get_matches();
    let source_addr: std::net::SocketAddr = format!("{}:{}",
                              matches.value_of("source").unwrap(),
                              matches.value_of("port").unwrap())
        .parse::<std::net::SocketAddr>().unwrap();
    let dst_addrs = matches.values_of("destinations").unwrap()
        .map(|d| -> std::net::SocketAddr {
            if d.contains(":") {
                return d.parse::<std::net::SocketAddr>().unwrap();
            } else {
                return format!("{}:{}", DEFAULT_DESTINATION_IP, d)
                    .parse::<std::net::SocketAddr>().unwrap();
            }
        }).collect::<Vec<std::net::SocketAddr>>();
    Args {
        source_addr: source_addr,
        dst_addrs: dst_addrs
    }
}

fn main() -> std::io::Result<()> {
    let args: Args = args_handler();
    let mainsocket: std::net::UdpSocket =
        std::net::UdpSocket::bind(args.source_addr).expect("Unable to bind source address");
    println!("Listening on {}...", args.source_addr);
    let mess_lock = std::sync::Arc::new(std::sync::RwLock::new(Message {
        size: 0,
        data: [0; std::u16::MAX as usize]
    }));
    let client_map_lock  = std::sync::Arc::new(std::sync::RwLock::new(
        std::collections::HashSet::new()));
    let consume_barrier = std::sync::Arc::new(std::sync::Barrier::new(args.dst_addrs.len() + 1));

    let _: Vec<std::thread::JoinHandle<()>> = args.dst_addrs.into_iter().map(|addr| {
        let c_mainsocket = mainsocket.try_clone().unwrap();
        let c_mess_lock = mess_lock.clone();
        let c_client_map_lock = client_map_lock.clone();
        let c_consumer_barrier = consume_barrier.clone();
        std::thread::spawn(move || {
            let socket: std::net::UdpSocket =
                std::net::UdpSocket::bind("0.0.0.0:0").unwrap();
            socket.connect(addr).unwrap();
            let c_socket = socket.try_clone().unwrap();
            let _ = std::thread::spawn(move || {
                loop {
                    c_consumer_barrier.wait();
                    let mess = c_mess_lock.read().unwrap();
                    match c_socket.send(&mess.data[..mess.size]) {
                        Err(_) => (),
                        _ => ()
                    }
                }
            });
            println!("Opening {}...", addr);
            let mut tmp_mess: [u8; std::u16::MAX as usize] =
                [0; std::u16::MAX as usize];
            loop {
                let size = match socket.recv(&mut tmp_mess) {
                    Ok(size) => size,
                    Err(_) => continue
                };
                let client_map = c_client_map_lock.read().unwrap();
                for addr in &*client_map {
                    let _ = c_mainsocket.send_to(&tmp_mess[..size], addr);
                }
            }
        })
    }).collect::<Vec<std::thread::JoinHandle<()>>>();
    let mut tmp_mess: [u8; std::u16::MAX as usize] = [0; std::u16::MAX as usize];
    loop {
        let (size, src_addr): (usize, std::net::SocketAddr) =
                               match mainsocket.recv_from(&mut tmp_mess) {
                                   Ok((size, src_addr)) => (size, src_addr),
                                   Err(_) => continue
                               };
        consume_barrier.wait();
        let mut mess = mess_lock.write().unwrap();
        mess.size = size;
        mess.data.copy_from_slice(&tmp_mess);
        let mut client_map = client_map_lock.write().unwrap();
        client_map.insert(src_addr);
    }
    Ok(())
}
